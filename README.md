Gua sha is a natural, different treatment that entails scratching your skin with a massage device to enhance your blood circulation.
This old Chinese healing strategy may offer a special method to much better health and wellness, addressing issues like chronic pain. 
In gua sha, a service technician scuffs your skin with short or long strokes to boost microcirculation of the soft tissue, which raises
blood circulation. They make these strokes with a smooth-edged tool referred to as a gua massage device. The service technician applies 
massage oil to your skin, and after that uses the device to repetitively scratch your skin in a downward movement. Gua sha is meant to
attend to stationary energy, called chi, in the body that practitioners think may be in charge of inflammation. Swelling is 
the underlying reason for a number of problems connected with persistent discomfort. Massaging the skin's surface area is thought to aid 
separate this power, lower inflammation, as well as advertise recovery. Gua sha is typically done on a person's back, butts, neck, arms, 
and legs. A mild version of it is also utilized on the face as a facial strategy. Your professional might use light pressure, as well as 
gradually increase strength to establish just how much pressure you can manage.

https://www.kallypaw.com/collections/health-and-beauty/products/rose-quartz-face-roller-gua-sha-set